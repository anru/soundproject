package com.soundproject;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Based on and adapted from https://github.com/jsierles/react-native-audio
 */
public class NoiseRecorderManager extends ReactContextBaseJavaModule {

    private static final String TAG = "NoiseRecorderManager";
    private static final double REFERENCE = 0.00002;

    private static final String TAG_CURRENT_TIME = "currentTime";
    private static final String TAG_CURRENT_DB = "currentDb";
    private static final String EVENT_RECORDING_PROGRESS = "recordingProgress";
    private static final String EVENT_RECORDING_FINISHED = "recordingFinished";

    private Context mContext;
    private MediaRecorder mRecorder;
    private boolean mIsRecording = false;
    private Timer mTimer;
    private int mCurrentTime;

    public NoiseRecorderManager(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
    }

    @Override
    public Map<String, Object> getConstants() {
        return new HashMap<>();
    }

    @Override
    public String getName() {
        return "NoiseRecorderManager";
    }

    @ReactMethod
    public void checkAuthorizationStatus(Promise promise) {
        int permissionCheck = ContextCompat.checkSelfPermission(getCurrentActivity(),
                Manifest.permission.RECORD_AUDIO);
        boolean permissionGranted = permissionCheck == PackageManager.PERMISSION_GRANTED;
        promise.resolve(permissionGranted);
    }

    @ReactMethod
    public void startRecording(Promise promise) {
        Log.d(TAG, "startRecording");

        if (mIsRecording || mRecorder != null) {
            Log.d(TAG, "startRecording already recording");
            stopRecording();
        }

        mRecorder = new MediaRecorder();
        try {
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile("/dev/null");
        }
        catch(final Exception e) {
            logAndRejectPromise(promise, "COULD_NOT_CONFIGURE_MEDIA_RECORDER",
                    "Make sure you've added RECORD_AUDIO permission to your AndroidManifest.xml file"
                            + e.getMessage());
            return;
        }

        try {
            mRecorder.prepare();
        } catch (final Exception e) {
            logAndRejectPromise(promise, "COULD_NOT_PREPARE_RECORDING", e.getMessage());
        }

        mRecorder.start();
        mIsRecording = true;

        startTimer();

        promise.resolve("");
    }

    @ReactMethod
    public void stopRecording(Promise promise) {
        Log.d(TAG, "stopRecording isRecording: " + mIsRecording);

        stopTimer();
        mIsRecording = false;

        if (mRecorder != null) {
            try {
                mRecorder.stop();
                mRecorder.release();
            } catch (final RuntimeException e) {
                logAndRejectPromise(promise, "RUNTIME_EXCEPTION", "No valid audio data received.");
                return;
            } finally {
                mRecorder = null;
            }
        }

        promise.resolve("");
        sendEvent(EVENT_RECORDING_FINISHED, null);
    }

    private void stopRecording() {
        stopTimer();
        mIsRecording = false;

        try {
            mRecorder.stop();
            mRecorder.release();
        }
        catch (final RuntimeException e) {
            Log.d(TAG, "Could not stop recording");
        }
        finally {
            mRecorder = null;
        }
    }

    private void startTimer() {
        stopTimer();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                WritableMap body = Arguments.createMap();
                body.putInt(TAG_CURRENT_TIME, mCurrentTime);

                double pressure = mRecorder.getMaxAmplitude()/51805.5336;
                double currentDb = (20 * Math.log10(pressure/REFERENCE));
                if (currentDb < 0.0d) {
                    currentDb = 0.0d;
                }
                body.putDouble(TAG_CURRENT_DB, currentDb);
                sendEvent(EVENT_RECORDING_PROGRESS, body);
                mCurrentTime += 100;
            }
        }, 0, 100);
    }

    private void stopTimer() {
        mCurrentTime = 0;
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
    }

    private DeviceEventManagerModule.RCTDeviceEventEmitter mModule = null;

    private void sendEvent(String eventName, Object params) {
        if (mModule == null) {
            mModule = getReactApplicationContext()
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class);
        }
        mModule.emit(eventName, params);
    }

    private void logAndRejectPromise(Promise promise, String errorCode, String errorMessage) {
        Log.e(TAG, errorMessage);
        promise.reject(errorCode, errorMessage);
    }

}
