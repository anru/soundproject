function renderCanvas(canvas) {

  var canvas_width = window.innerWidth;
  var canvas_height = 200;
  var w = canvas_width, h = canvas_height;
  var gridlines = [];
  var marginBottom = 25;
  var marginLeft = 40;
  var marginRight = 10;
  var points = [];
  var position = 0;
  var previousLength = -1;
  var maxPointsLength = canvas_width - marginLeft - marginRight;

  // http://stackoverflow.com/questions/15661339/how-do-i-fix-blurry-text-in-my-html5-canvas
  var ctx = canvas.getContext('2d')
  var dpr = window.devicePixelRatio || 1;
  var bsr = ctx.webkitBackingStorePixelRatio
          || ctx.mozBackingStorePixelRatio
          || ctx.msBackingStorePixelRatio
          || ctx.oBackingStorePixelRatio
          || ctx.backingStorePixelRatio || 1;
  var ratio = dpr / bsr;

  canvas.width = canvas_width * ratio;
  canvas.height = canvas_height * ratio;
  canvas.style.left = '0px';
  canvas.style.top = '0px';
  canvas.style.width = canvas_width + "px";
  canvas.style.height = canvas_height + "px";
  canvas.style.padding = "0px 0px 0px 0px"

  ctx.setTransform(ratio, 0, 0, ratio, 0, 0);

  function Label(text, x, y) {
      this.text = text;
      this.x = x; // - ctx.measureText(text).width;
      this.y = y; // + ctx.measureText(text).height / 2;
  }
  
  function GridLine(label, x, y, x1, y1, thickness, color) {
    this.x = x;
    this.y = y;
    this.vx = x1;
    this.vy = y1;
    this.label = label;
	this.thickness = thickness;
	this.rgba = color;
  }

  function Point(x, y) {
    this.x = x;
    this.y = y;
  }

  function draw() {
    ctx.clearRect(0, 0, w, h);
    // ctx.globalCompositeOperation = 'lighter';
    ctx.font="16px roboto, sans-serif";

    // ctx.strokeStyle = '#333333';
    // ctx.lineWidth = 0.5;
    // ctx.fillText("window.innerWidth: " + window.innerWidth, 10, 14);
	// ctx.fillText("canvas.width : " + canvas.width , 10, 28);

    for (var i = 0; i < gridlines.length; i++) {
        var gridLine = gridlines[i];
        ctx.lineWidth = gridLine.thickness;
        ctx.strokeStyle = gridLine.rgba;
        ctx.beginPath();
        ctx.moveTo(gridLine.x, gridLine.y);
        ctx.lineTo(gridLine.vx, gridLine.vy);
        ctx.stroke();
        ctx.closePath();
		/*
		ctx.textAlign = "start";
        ctx.strokeStyle = '#333333';
		ctx.fillStyle = "#333333";
        ctx.lineWidth = 0.1;
		 */
		var label = gridLine.label;
		if (label) {
            ctx.fillText(label.text, label.x - ctx.measureText(label.text).width, label.y + 6);
        }
    }

    if (points.length > 0) {
        var point = points[0];
		ctx.lineWidth = 0.5;
        ctx.strokeStyle = '#FF0000';
        ctx.beginPath();
        ctx.moveTo(marginLeft, point.y);
        for (var i = 0; i < points.length; i++) {
            point = points[i];
            ctx.lineTo(marginLeft + i, point.y);
        }
        ctx.stroke();
        ctx.closePath();
    }
  }

  window.requestAnimFrame = (function() {
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            function( callback ) {
              window.setTimeout(callback, 1000 / 60);
            };
  })();

  document.addEventListener('message', function(event) {
    if (points.length >= maxPointsLength) {
        points.shift();
    }

    var value = h - event.data - marginBottom;
    points.push(new Point(position++, value));
    draw();
  }, false);

  (function init() {
    gridlines.push(new GridLine(null, 0, h, w, h, 0.1, '#333333'));
    gridlines.push(new GridLine(new Label("0", 35, h - marginBottom), marginLeft, h - marginBottom, w, h - marginBottom, 0.1, '#333333'));
    gridlines.push(new GridLine(new Label("30", 35, h - marginBottom - 30), marginLeft, h - marginBottom - 30, w, h - marginBottom - 30, 0.1, '#333333'));
    gridlines.push(new GridLine(new Label("60", 35, h - marginBottom - 60), marginLeft, h - marginBottom - 60, w, h - marginBottom - 60, 0.1, '#333333'));
    gridlines.push(new GridLine(new Label("120", 35, h - marginBottom - 120), marginLeft, h - marginBottom - 120, w, h - marginBottom - 120, 0.1, '#333333'));
	
	var ta = h - marginBottom - 120 - 3;
	var ba = marginBottom - 3;
	var section5secods = (maxPointsLength) / 6;
	gridlines.push(new GridLine(null, marginLeft + section5secods, ta, marginLeft + section5secods, h - ba, 0.1, '#333333'));
	gridlines.push(new GridLine(null, marginLeft + 2 * section5secods, ta, marginLeft + 2 * section5secods, h - ba, 0.1, '#333333'));
	gridlines.push(new GridLine(null, marginLeft + 3 * section5secods, ta, marginLeft + 3 * section5secods, h - ba, 0.1, '#333333'));
	gridlines.push(new GridLine(null, marginLeft + 4 * section5secods, ta, marginLeft + 4 * section5secods, h - ba, 0.1, '#333333'));
	gridlines.push(new GridLine(null, marginLeft + 5 * section5secods, ta, marginLeft + 5 * section5secods, h - ba, 0.1, '#333333'));
    gridlines.push(new GridLine(null, (maxPointsLength + marginLeft), ta, (maxPointsLength + marginLeft), h - ba, 0.1, '#333333'));
  })();

  (function loop(){
    draw();
    requestAnimFrame(loop);
  })();
}