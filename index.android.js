/**
 * Noise level metering app to test React Native framework
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  WebView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Animated,
  View,
  Image,
  Platform,
  PermissionsAndroid,
  Easing
} from 'react-native';

import InfoView from './InfoView';
import {NoiseRecorder} from './NoiseRecorderManager';

const Dimensions = require('Dimensions');

class ImageButton extends React.Component {

  _handlePress = () => {
    if (this.props.enabled !== false && this.props.onPress) {
      this.props.onPress();
    }
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={this._handlePress}>
        <View>
          <Image style={styles.image_button} source={this.props.source}/>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default class SoundProject extends Component {

    state = {
      currentTime: 0.0,
      lastDb: 0.0,
      currentDb: 0.0,
      minDb: 120.0,
      avgDb: 0.0,
      maxDb: 0.0,
      recording: false,
      stoppedRecording: false,
      finished: false,
      hasPermission: undefined,
      spinValue: new Animated.Value(0),
    };

  toggleRecording = () => {
    if (this.state.recording) {
      console.log("SoundProject#toggleRecording stopping");
      NoiseRecorder.stopRecording();
      this.state.recording = false;
    }
    else {
      NoiseRecorder.startRecording();
      this.state.recording = true;
      console.log("SoundProject#toggleRecording starting");
    }
  }

  stopRecording = () => {
    console.log("SoundProject#stopRecording stopping");
    NoiseRecorder.stopRecording();
    this.state.recording = false;
  }

  componentWillMount() {
    console.log("SoundProject#componentWillMount");
  }
  
  componentWillUnmount() {
    console.log("SoundProject#componentWillUnmount");
    if (this.state.recording) {
      this.stopRecording();
    }
  }
  
  componentWillUpdate() {
    console.log("SoundProject#componentWillUpdate");
    if (this.state.recording) {
      this.stopRecording();
    }
  }

  onMessage = () => {
    console.log("SoundProject#onMessage");
  }

  constructor(props) {
    super(props);
  }

  spin() {
    this.state.spinValue.setValue(this.state.lastDb); 
    Animated.timing(this.state.spinValue, {
      	toValue: this.state.currentDb,
        duration: 90,
        easing: Easing.linear
      }
    ).start()
  }

  componentDidMount() {
    console.log("SoundProject#componentDidMount");
    this._checkPermission().then((hasPermission) => {
      this.setState({ hasPermission });

      if (!hasPermission) return;

      NoiseRecorder.onProgress = (data) => {
        console.log(data);
        if (this.webview && data.currentDb > 0) {
          this.webview.postMessage(data.currentDb);
        }
        this.state.lastDb = this.state.currentDb;
        this.state.currentDb = data.currentDb;

        var min = this.state.minDb;
        var avg = this.state.avgDb;
        var max = this.state.maxDb;
		var currentDb = this.state.currentDb ? this.state.currentDb.toFixed(1) : 0.0;
        if (max < this.state.currentDb) {
            max = this.state.currentDb.toFixed(2)
        }
        if (min > this.state.currentDb && data.currentDb > 0) {
            min = this.state.currentDb.toFixed(2)
        }
        if (!max) {
           max = 0.0;
        }
        if (!min) {
           min = 0.0;
        }
        avg = ((max - min) / 2).toFixed(2);

        this.spin();

        this.state.minDb = min;
        this.state.avgDb = avg;
        this.state.maxDb = max;
        this.state.currentTime = data.currentTime;
        if (this.infoview) {
            this.infoview.setState({currentDb: currentDb, minDb: min, avgDb: avg,
                maxDb: max, currentTime: data.currentTime});
        }
      };

      NoiseRecorder.onFinished = (data) => {
      };
	  
      // this.toggleRecording();
    });
  }

  _checkPermission() {
      if (Platform.OS !== 'android') {
        return Promise.resolve(true);
      }

      const rationale = {
        'title': 'Microphone Permission',
        'message': 'SoundPrject needs access to your microphone so you can record audio.'
      };

      return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, rationale)
        .then((result) => {
          console.log('Permission result:', result);
          return (result === true || result === PermissionsAndroid.RESULTS.GRANTED);
      });
  }

  render() {

    const spin = this.state.spinValue.interpolate({
       inputRange: [0, 120],
       outputRange: ['0deg', '180deg']
    });

    console.log("SoundProject#render");
    return (
      <View style={styles.container}>
        <View style={{flex: 2, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.header}>Sound meter</Text>
          <Text style={styles.instructions}>Test application for framework. Measurements not accurate.</Text>
          <View style={{flex: 1, flexDirection: 'row'}}>
           <View style={{left: 0, width: Dimensions.get('window').width, height: 400, alignItems: 'center', backgroundColor: '#F5FCFF'}}>
             <Image style={{left:0, top:0, width: 320, height:190, resizeMode: 'contain'}} source={require('./images/bg_scale.png')} />
             <InfoView ref={infoview => { this.infoview = infoview; }} />
           </View>
           <View style={{position: 'absolute', width: Dimensions.get('window').width, height: 16, top: 158, alignItems: 'center',}}>
             <Animated.Image style={{left:0, top:0, width: 300, transform: [{rotate: spin}]}} source={require('./images/ic_arrow.png')} />
           </View>
           <View style={styles.floatView1}>
             <ImageButton tyle={{left:0, top:0}} onPress={this.toggleRecording} enabled source={require('./images/ic_pause.png')}/>
           </View>
          </View>
        </View>
        <View style={{height: 200, backgroundColor: '#F5FC00'}}>
          <WebView
            ref={webview => { this.webview = webview; }}
            automaticallyAdjustContentInsets={false}
            contentInset={{top: 0, right: 0, bottom: 0, left: 0}}
            opaque={false}
            source={{uri: 'file:///android_asset/canvas.html'}}
            
            underlayColor={'transparent'}
            onMessage={this.onMessage}
            style={{height: 200, width: Dimensions.get('window').width, backgroundColor: '#F5FCFF'}}
          />
        </View>
        <View style={{height: 30, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.instructions}>footer</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  header: {
    fontSize: 24,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    flex: 0.5,
    width: 0,
    margin: 5,
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: 'gray',
  },
  buttons: {
    flexDirection: 'row',
    height: 30,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image_button: {
    width: 50,
    height: 50,
    margin: 10
  },
  floatView: {
    position: 'absolute',
    width: 300,
    height: 16,
    top: 158,
    left: 30,
  },
  floatView1: {
    position: 'absolute',
    width: 60,
    height: 60,
  },
});

AppRegistry.registerComponent('SoundProject', () => SoundProject);
