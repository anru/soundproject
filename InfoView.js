'use strict';

import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';

export default class InfoView extends Component {

    state = {
        currentTime: 0.0,
        lastDb: 0.0,
        currentDb: 0.0,
        minDb: 120.0,
        avgDb: 0.0,
        maxDb: 0.0,
    };
	
	millisToTime(millis) {
	    var hours = Math.floor(millis / 36e5),
	        mins = Math.floor((millis % 36e5) / 6e4),
	        secs = Math.floor((millis % 6e4) / 1000);
	    return (('00' + mins).slice(-2) + ':' + ('00' + secs).slice(-2));  
	}

    componentDidMount() {
        console.log("InfoView#componentDidMount");
    }

    render() {
		// console.log("InfoView#render");
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
			    <View style={{flex: 0, flexDirection: 'row', top:20, left:0, alignItems: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}} />
			        <View style={{flex: 1, flexDirection: 'row'}}>   
			            <Text textAlign='right' style={{fontSize: 14, color: '#555555', marginBottom: 5, alignItems: 'center'}}>{this.millisToTime(this.state.currentTime)}</Text>
			        </View>
			    </View>
                <View style={{flex: 0, flexDirection: 'row', top:15, left:0, alignItems: 'center'}}>
			        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>  
                        <Text textAlign='left' style={{fontSize: 30, color: '#333333', marginBottom: 5}}>{this.state.currentDb}</Text>
			        </View>
			        <View style={{flex: 1, flexDirection: 'row'}}>   
			            <Text textAlign='right' style={{fontSize: 30, color: '#333333', marginBottom: 5}}>dB</Text>
			        </View>
			    </View>
                <View style={{opacity:0, padding: 10, flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>  
                    <Text style={{fontSize: 20, top:0, left:0, color: '#555555', marginBottom: 5,}}>Min:{this.state.minDb}</Text>
                    <Text style={{fontSize: 20, top:0, left:0, color: '#555555', marginBottom: 5,}}>Avg:{this.state.avgDb}</Text>
                    <Text style={{fontSize: 20, top:0, left:0, color: '#555555', marginBottom: 5,}}>Max:{this.state.maxDb}</Text>
                </View>
            </View>
        );
    }
};

InfoView.propTypes = {
};
