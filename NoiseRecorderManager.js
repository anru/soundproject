'use strict';

import React from "react";
import ReactNative, {
  NativeModules,
  NativeAppEventEmitter,
  DeviceEventEmitter,
  Platform
} from "react-native";

var NoiseRecorderManager = NativeModules.NoiseRecorderManager;


var NoiseRecorder = {
  startRecording: function() {
    if (this.progressSubscription) this.progressSubscription.remove();
    this.progressSubscription = NativeAppEventEmitter.addListener('recordingProgress',
      (data) => {
        if (this.onProgress) {
          this.onProgress(data);
        }
      }
    );

    if (this.finishedSubscription) this.finishedSubscription.remove();
    this.finishedSubscription = NativeAppEventEmitter.addListener('recordingFinished',
      (data) => {
        if (this.onFinished) {
          this.onFinished(data);
        }
      }
    );

    return NoiseRecorderManager.startRecording();
  },
  stopRecording: function() {
    return NoiseRecorderManager.stopRecording();
  },
  checkAuthorizationStatus: NoiseRecorderManager.checkAuthorizationStatus,
  removeListeners: function() {
    if (this.progressSubscription) this.progressSubscription.remove();
    if (this.finishedSubscription) this.finishedSubscription.remove();
  },
};

module.exports = {NoiseRecorder};
